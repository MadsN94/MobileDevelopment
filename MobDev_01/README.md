### 1. Introduction (1m 12s)
https://www.lynda.com/AngularJS-tutorials/Building-Mobile-App-AngularJS-1-Ionic/368920-2.html
What you should know before watching this course (1m 6s)
https://www.lynda.com/AngularJS-tutorials/What-you-should-know-before-watching-course/368920/386099-4.html?
Understanding our mobile framework (3m 45s)
https://www.lynda.com/AngularJS-tutorials/Understanding-our-mobile-framework/368920/386102-4.html?
Installing development environment on a pc (4m 27s)
https://www.lynda.com/AngularJS-tutorials/Installing-development-environment-PC/368920/386104-4.html?
-	Installer NodeJS
-	Installer Git (Hvis ikke allerede installeret)
Understanding Ionic installation (5m 45s)
https://www.lynda.com/AngularJS-tutorials/Understanding-Ionic-installation/368920/386105-4.html?
Working with the Ionic CLI and templates (10m 2s)
https://www.lynda.com/AngularJS-tutorials/Working-Ionic-CLI-templates/368920/386106-4.html?
Understanding basic Ionic CSS components (7m 37s)
https://www.lynda.com/AngularJS-tutorials/Understanding-basic-Ionic-CSS-components/368920/386108-4.html?
Using Ionic buttons (5m 14s)
https://www.lynda.com/AngularJS-tutorials/Using-Ionic-buttons/368920/386109-4.html?
Adding Ionicicons to a layout (8m 42s)
https://www.lynda.com/AngularJS-tutorials/Adding-Ionicons-layout/368920/386110-4.html?
Working with tabs (10m 3s)
https://www.lynda.com/AngularJS-tutorials/Working-tabs/368920/386111-4.html?
Using the list styles (12m 30s)
https://www.lynda.com/AngularJS-tutorials/Using-list-styles/368920/386112-4.html?
Creating cards and inset lists (4m 46s)
https://www.lynda.com/AngularJS-tutorials/Creating-cards-inset-lists/368920/386113-4.html?
Creating basic form elements (5m 1s)
https://www.lynda.com/AngularJS-tutorials/Creating-basic-form-elements/368920/386114-4.html?
Using other form types (7m 35s)
https://www.lynda.com/AngularJS-tutorials/Using-other-form-types/368920/386115-4.html?
Using special form elements (5m 16s)
https://www.lynda.com/AngularJS-tutorials/Using-special-form-elements/368920/386116-4.html?
